﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(CharacterMovement))]
[RequireComponent(typeof(MouseLook))]
public class PlayerFPSController : MonoBehaviour
{

    private CharacterMovement characterMovement;
    private MouseLook mouseLook;
    private GunAiming gunAiming;
    private FireWeapon fireWeapon;

    void Start()
    {

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Body").GetComponent<MeshRenderer>().enabled = false;

        characterMovement = GetComponent<CharacterMovement>();
        mouseLook = GetComponent<MouseLook>();
        gunAiming = GetComponentInChildren<GunAiming>();

        fireWeapon = GetComponentInChildren<FireWeapon>();
    }

    void Update()
    {
        movement();
        rotation();
        aiming();
        shooting();
    }

    private void movement()
    {
        // Movement
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovement, vMovement, jumpInput, dashInput);
    }

    private void rotation()
    {
        // Rotation
        float hRotation = Input.GetAxis("Mouse X");
        float vRotation = Input.GetAxis("Mouse Y");

        mouseLook.handleRotation(hRotation, vRotation);

    }

    private void aiming()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            gunAiming.OnButtonDown();
        }
        else if (Input.GetButtonUp("Fire2"))
        {
            gunAiming.OnButtonUp();
        }
    }

    private void shooting()
    {

        //Reloading goes first
        if (Input.GetKeyDown(KeyCode.R))
        {
            fireWeapon.OnReloadButtonDown();
        } else
        {
            switch (fireWeapon.gunData.firetype)
            {
                case FIRETYPE.REPEATER:
                case FIRETYPE.SEMIAUTOMATIC:
                    fireWeapon.shoot(Input.GetButtonDown("Fire1"));
                    break;

                case FIRETYPE.AUTOMATIC:
                    fireWeapon.shoot(Input.GetButton("Fire1"));
                    break;
            }
        }
    }
}
